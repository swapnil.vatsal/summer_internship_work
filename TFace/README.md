## Introduction

This is a handy data-augmentation tool for adding hijabs and turbans to faces.

## Usage 

### Installing the required packages

```bash
pip install -r requirements.txt
```
### Driver Code
#### TFace

Place all the images in TFace/torchkit/augmentation/face_68_feactures_dlib/input/ (Remove .gitkeep file if present in this directory)

```bash
cd summer_internship_work/TFace/torchkit/augmentation/
python landmark_AddHeadband2Face.py --type_of_headwear <enter_index_of_headwear from headband_list.txt(0 based indexing)> --abs_dir <enter_the_absolute_directory_of_the_tool>
```
example usage
```bash
cd summer_internship_work/TFace/torchkit/augmentation/
python landmark_AddHeadband2Face.py --type_of_headwear 7 --abs_dir '/home/swapnil/TFace/torchkit/augmentation/'
```
After this remove the files imagelist.txt and keypointslist.txt
The modified files can be found in augmentation/output/input/

#### MasktheFace

```
cd MaskTheFace
# Generic
python mask_the_face.py --path <path-to-file-or-dir> --mask_type <type-of-mask> --verbose --write_original_image

# Example
python mask_the_face.py --path 'data/office.jpg' --mask_type 'N95' --verbose --write_original_image
```

#### Arguments
|    Argument    |                                                                                                       Explanation                                                                                                       |
|:--------------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|      path      |                                                                            Path to the image file or a folder containing images to be masked                                                                            |
|    mask_type   | Select the mask to be applied. Available options are 'N95', 'surgical_blue', 'surgical_green', 'cloth', 'empty' and 'inpaint'. The details of these mask types can be seen in the image above. More masks will be added |
|     pattern    |                                 Selects the pattern to be applied to the mask-type selected above. The textures are available in the masks/textures folder. User can add more textures.                                 |
| pattern_weight |                                   Selects the intensity of the pattern to be applied on the mask. The value should be between 0 (no texture strength) to 1 (maximum texture strength)                                   |
|      color     |                                                         Selects the color to be applied to the mask-type selected above. The colors are provided as hex values.                                                         |
|  color_weight  |                                      Selects the intensity of the color to be applied on the mask. The value should be between 0 (no color strength) to 1 (maximum color strength)                                      |
|      code      |                                                              Can be used to create specific mask formats at random. More can be found in the section below.                                                             |
|     verbose    |                                                                          If set to True, will be used to display useful messages during masking                                                                         |
|write_original_image|                   If used, the original unmasked image will also be saved in the masked image folder along with processed masked image                                                                              |


