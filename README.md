## Introduction

The entire project revolves around adding occlusions to face which can befuddle an AI-based face recognition model. Generation of occluded faces can help in training an FR model. TFace was used to generate hijabs and turbans. MaskTheFace is used to add masks to faces.

## Usage 

### Installing the required packages

```bash
pip install -r requirements.txt
```
### Driver Code
### TFace

Place all the images in TFace/torchkit/augmentation/face_68_feactures_dlib/input/ (Remove .gitkeep file if present in this directory).Ensure that we are placing png/jpg files and not any other file extensions or folders.

```bash
cd summer_internship_work/TFace/torchkit/augmentation/
rm -r face_68_feactures_dlib/input/.gitkeep
rm -r face_68_feactures_dlib/output/landmarks_txt/.gitkeep
python landmark_AddHeadband2Face.py --type_of_headwear <enter_index_of_headwear from headband_list.txt(0 based indexing)> --abs_dir <enter_the_absolute_directory_of_the_tool>
```
example usage
```bash
cd summer_internship_work/TFace/torchkit/augmentation/
rm -r face_68_feactures_dlib/input/.gitkeep
rm -r face_68_feactures_dlib/output/landmarks_txt/.gitkeep
python landmark_AddHeadband2Face.py --type_of_headwear 7 --abs_dir '/home/swapnil/summer_internship_work/TFace/torchkit/augmentation/'
```
After this remove the files imagelist.txt and keypointslist.txt
The modified files can be found in augmentation/output/input/

<img src = "TFace/samples/Jennifer_Aniston_0020_orig.jpg">
<img src = "TFace/samples/Jennifer_Aniston_0020.jpg">

##### For turbans (Same procedure for Eyeglasses)
Simply copy the .jpg files in TFace/torchkit/augmentation/headband/turbans to the headband folder and the 
files in TFace/torchkit/augmentation/headband/turbans/turban_test_pts/ to TFace/torchkit/augmentation/headband_test_pts

##### For custom objects

To add custom objects to faces follow this procedure : 
1. Open <a href = "https://www.makesense.ai/">Makesense</a>, and click on Get Started.
2. Upload the object to add to faces and go to Object Detection.
3. Start a new project, go to points and start marking the 68 points corresponding to the 68 points of faces.
<img src = "TFace/samples/dlib_68_points.jpg"></img>

4. Download the coordinates as a csv file and convert it to a .jpg file(Not a normal .jpg file but a .txt file with an extension of .jpg instead of .txt, See the files in TFace/torchkit/augmentation/headband_test_pts for reference).
5. Copy the object image to TFace/torchkit/augmentation/headband and the the file in 4th step to TFace/torchkit/augmentation/headband_test_pts.
6. Run the script.

### MasktheFace

```
cd summer_internship_work/MaskTheFace
# Generic
python mask_the_face.py --path <path-to-file-or-dir> --mask_type <type-of-mask> --verbose --write_original_image

# Example
python mask_the_face.py --path 'data/office.jpg' --mask_type 'N95' --verbose --write_original_image
```

#### Arguments
|    Argument    |                                                                                                       Explanation                                                                                                       |
|:--------------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|      path      |                                                                            Path to the image file or a folder containing images to be masked                                                                            |
|    mask_type   | Select the mask to be applied. Available options are 'N95', 'surgical_blue', 'surgical_green', 'cloth', 'empty' and 'inpaint'. The details of these mask types can be seen in the image above. More masks will be added |
|     pattern    |                                 Selects the pattern to be applied to the mask-type selected above. The textures are available in the masks/textures folder. User can add more textures.                                 |
| pattern_weight |                                   Selects the intensity of the pattern to be applied on the mask. The value should be between 0 (no texture strength) to 1 (maximum texture strength)                                   |
|      color     |                                                         Selects the color to be applied to the mask-type selected above. The colors are provided as hex values.                                                         |
|  color_weight  |                                      Selects the intensity of the color to be applied on the mask. The value should be between 0 (no color strength) to 1 (maximum color strength)                                      |
|      code      |                                                              Can be used to create specific mask formats at random. More can be found in the section below.                                                             |
|     verbose    |                                                                          If set to True, will be used to display useful messages during masking                                                                         |
|write_original_image|                   If used, the original unmasked image will also be saved in the masked image folder along with processed masked image                                                                              |

## Supported Masks:
### Mask Types:
Currently MaskTheFace supports the following 4 mask types
1. Surgical
2. N95
3. KN95
4. Cloth
5. Gas

![mask_types](MaskTheFace/images/mask_types.png)

This readme file neatly summarizes the usage of the tools. For details of MaskTheFace you can see the readme.md file in MaskTheFace.

